use std::cell::{Ref,RefCell};
use std::rc::Rc;
use std::fmt::{Formatter,Display,Debug};
use std::fmt;
use std::rc::Weak;
use std::result::Result;
enum Errors{
    NoTreePath
}
#[derive(Debug,Eq,PartialEq)]
struct Tree<T>{
    data : T,
    children : Vec<Tree<T>>
}
impl <T> Tree<T>{
    fn new(dat : T) -> Self{
        Tree{data : dat, children : Vec::new()}
    }
    fn new_children(dat : T, child : Vec<Tree<T>>) -> Self{
        Tree{data : dat, children : child}
    }

    fn add_child(&mut self, child : Tree<T>) -> (){
        self.children.push(child);
    }

    fn get_value(&self) -> &T{
        &self.data
    }

    fn nodes(&self) -> usize{
        if self.children.is_empty(){
            1
        }
        else{
            let mut x = 1;
            for child in &self.children{
                x +=  child.nodes();
            }
            x
        }
    }

    fn children_number(&self) -> usize{
        self.children.len()
    }

    fn height(&self) -> usize{
        if self.children.is_empty(){
            0
        }
        else{
            let mut height = 0;
            for child in &self.children{
                let t = child.height();
                if t > height{
                    height = t;
                }
            }
            height + 1
        }
    }

    fn follow_path(&self, path : &[usize]) -> Result<&Tree<T>, Errors>{
        let mut curr = self;
        for i in path {
            if curr.children.len() >= *i {
                return Err(Errors::NoTreePath);
            } else {
                curr = &curr.children[*i -1];
            }
        }
        Ok(curr)
    }

}

fn main(){
    let mut tree = Tree::new(0);
    let mut tree1 = Tree::new(10);
    let mut tree2 = Tree::new(2);
    tree1.add_child(Tree::new(12));
    tree2.add_child(tree1);
    tree.add_child(Tree::new(12));
    tree.add_child(tree2);

    println!("{:?}", tree.get_value());
}

#[derive(Debug)]
struct Node<T>{
    data : T,
    succ : Option<Rc<RefCell<Node<T>>>>
}

impl <T> Node<T>{
    fn new(val : T) -> Self{
        Node{data : val, succ : None}
    }

    fn set_succ(&mut self, succ : Rc<RefCell<Node<T>>>) -> (){
        self.succ = Some(succ);
    }
}
#[derive(Debug)]
enum ERROR{
    CODA_VUOTA
}
#[derive(Debug)]
struct Queue<T>{
    head : Option<Rc<RefCell<Node<T>>>>,
    tail : Option<Weak<RefCell<Node<T>>>>
}


impl <T> Queue<T>{
    fn new() -> Self{
        Queue{head : None, tail : None}
    }
    fn insert(&mut self, val : T) -> (){
        if let None = self.head{
            let ins = Rc::new(RefCell::new(Node::new(val)));
            self.tail = Some(Rc::downgrade(&ins));
            self.head = Some(ins);

        }
        else{
            let ins = Rc::new(RefCell::new(Node::new(val)));
            let mut weak_last = self.tail.take().unwrap();
            let mut up_last = weak_last.upgrade().unwrap();
            self.tail = Some(Rc::downgrade(&ins));
            up_last.borrow_mut().set_succ(ins);

        }
    }

    fn remove(&mut self) -> Result<T,ERROR>{
        if let None = self.head{
            Err(ERROR::CODA_VUOTA)
        }
        else{
            let own = self.head.take().unwrap();
            let mut bor = Rc::try_unwrap(own).unwrap_or_else(move |kk| panic!()).into_inner();
            let succs = bor.succ.take();
            self.head = succs;
            Ok(bor.data)
        }
    }
}


impl <T:Display> Display for Queue<T>{
    fn fmt(&self, f : &mut Formatter) -> fmt::Result{
        let mut string = "[".to_string();
        if let None = self.head {
            string = string + "]";
        }
        else {
            let mut opt = self.head.clone().unwrap();
            let mut cl = opt.borrow();
            string = string + &format!("{}", cl.data);
            let mut succs = cl.succ.clone();
            loop {
                if let None = succs{
                    string = string + "]";
                    break;
                }
                else{
                    let opt1 = succs.unwrap();
                    let cl1 = opt1.borrow();
                    string = string + ", " + &format!("{}",cl1.data);
                    succs = cl1.succ.clone();
                }
            }
        }
        write!(f, "{}", string)
    }
}

fn main_queue() {
    let mut queue : Queue<u32> = Queue::new();
    for i in 0..100000{
        queue.insert(i);
    }
    println!("{}", queue);

    let mut i = 0;
    loop{
        let res = queue.remove();
        if let Ok(s) = res{
            i+=1;
            println!("{}",s);
        }
        else{
            println!("exception {:?}, removed {} elements", res, i);
            break;
        }
    }
}
